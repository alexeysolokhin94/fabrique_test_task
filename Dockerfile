FROM python:3.10-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN mkdir /fabrique_test_task
RUN pip install --upgrade pip


COPY requirements.txt /fabrique_test_task/
COPY mailing /fabrique_test_task/mailing/

RUN python -m pip install -r /fabrique_test_task/requirements.txt

WORKDIR /fabrique_test_task/mailing

RUN python manage.py migrate
RUN python manage.py loaddata tags.json
RUN python manage.py loaddata codes.json

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]