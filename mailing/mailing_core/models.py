from django.db import models
from django.utils.timezone import now
from django.core.validators import RegexValidator
import pytz


class Tags(models.Model):
    name = models.CharField(verbose_name='тэг', max_length=30)
    description = models.CharField(verbose_name='описание тэга', max_length=200)

    class Meta:
        verbose_name = 'тэг'
        verbose_name_plural = 'тэги'

    def __str__(self):
        return f'{self.name}'


class OperatorsCodes(models.Model):
    code = models.IntegerField(verbose_name='Код оператора')
    operator = models.CharField(verbose_name='оператор', max_length=50)

    class Meta:
        verbose_name = 'код оператора'
        verbose_name_plural = 'коды оператора'

    def __str__(self):
        return f'{self.operator}: {self.code}'


class Mailings(models.Model):
    """
    Модель, описывающая рассылку
    """

    start_time = models.DateTimeField(verbose_name='дата и время запуска', default=now)
    end_time = models.DateTimeField(verbose_name='дата и время окончания')
    message_text = models.TextField(verbose_name='текст сообщения')
    tags_filter = models.ManyToManyField(Tags, verbose_name='фильтр по тегам')
    codes_filter = models.ManyToManyField(OperatorsCodes, verbose_name='фильтр по кодам операторов')

    @property
    def short_text(self):
        return self.message_text[:30]

    class Meta:
        verbose_name = 'рассылка'
        verbose_name_plural = 'рассылки'
        ordering = ['start_time']

    def __str__(self):
        return f'Рассылка {self.start_time}: {self.message_text[:20]}'


class Clients(models.Model):
    """
    Модель, описывающая клиента
    """

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(verbose_name='номер телефона',
                                    max_length=11,
                                    unique=True,
                                    validators=[
                                        RegexValidator(r'7[0-9]{10}',
                                                       message='Введите номер телефона в формате "7XXXXXXXXXX"')
                                    ])
    operator_code = models.ForeignKey(OperatorsCodes, on_delete=models.CASCADE, verbose_name='код оператора',
                                      related_name='clients')
    tag = models.ForeignKey(Tags, on_delete=models.CASCADE, verbose_name='тэг', related_name='clients')
    timezone = models.CharField(verbose_name='часовой пояс', max_length=32, choices=TIMEZONES)

    class Meta:
        verbose_name = 'клиент'
        verbose_name_plural = 'клиенты'

    def __str__(self):
        return f'Клиент {self.phone_number}'


class Messages(models.Model):
    """
    Модель, описывающая сообщение
    """

    STATUS_CHOICES = (
        (0, 'Отправлено'),
        (1, 'Отправка не удалась')
    )

    sending_time = models.DateTimeField(verbose_name='время отправки', blank=True, null=True)
    message_status = models.IntegerField(verbose_name='статус отправки', choices=STATUS_CHOICES, default=1)
    client = models.ForeignKey(Clients, on_delete=models.CASCADE, verbose_name='получатель', related_name='messages')
    mailing = models.ForeignKey(Mailings, on_delete=models.CASCADE, verbose_name='рассылка', related_name='messages')

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'сообщения'

    def __str__(self):
        return f'Сообщение для {self.client.phone_number}: {self.message_status} в {self.sending_time}'
