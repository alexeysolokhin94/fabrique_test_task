import logging
from django.db.models.signals import post_save
from django.dispatch import receiver
from mailing_core.models import Mailings
from mailing.tasks import init_messages_creation

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Mailings)
def plan_mailing(sender, instance, created, **kwargs):
    """
    Планирование рассылки
    """

    if created:
        init_messages_creation.apply_async((instance.id,), eta=instance.start_time, expires=instance.end_time,
                                           retry=True)
        logger.info(f'new mailing added: {instance}')
