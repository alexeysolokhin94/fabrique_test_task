import requests
import json
import logging
from datetime import datetime
from mailing.settings import SENDER_TOKEN
from mailing_core.models import Messages, Clients, Mailings

logger = logging.getLogger(__name__)


def create_message(client: Clients, mailing: Mailings) -> Messages:
    """
    Создание и сохранение нового объекта сообщения

    :param client: объект клиента
    :type client: Clients
    :param mailing: объект рассылки
    :type mailing: Mailings

    :return: объект сообщения
    :rtype: Messages
    """

    new_message = Messages.objects.create(client=client, mailing=mailing, message_status=1, sending_time=datetime.now())
    new_message.save()
    return new_message


def message_update(message: Messages) -> None:
    """
    Обновление статуса сообщения после отправки.

    :param message: Объект сообщения
    :type message: Messages

    :return: None
    """

    message.message_status = 0
    message.sending_time = datetime.now()
    message.save()


def send_message(message: Messages) -> None:
    """
    Отправка сообщения на внешний сервис

    :param message: Объект сообщения
    :type message: Messages

    :return: None
    """

    base_url = 'https://probe.fbrq.cloud/v1/send/'
    headers = {'Authorization': 'Bearer ' + SENDER_TOKEN,
               'Content-Type': 'application/json'}
    message_id = message.id
    phone = message.client.phone_number
    message_text = message.mailing.message_text

    message_dict = {
        "id": message_id,
        "phone": phone,
        "text": message_text,
        }
    message_json = json.dumps(message_dict)

    response = requests.post(url=f'{base_url}{message_id}',
                             headers=headers,
                             data=message_json,
                             timeout=10)

    if response.status_code == 200:
        message_update(message)
    else:
        logger.error(f'Не удалось отправить сообщение {message_id}. Status Code: {response.status_code}')
