from django.contrib import admin
from mailing_core.models import Clients, Mailings, Messages, Tags, OperatorsCodes


@admin.register(Clients)
class ClientsAdmin(admin.ModelAdmin):
    """
    Регистрация и отображения модели Clients в админке
    """

    list_display = ['phone_number', 'operator_code', 'tag', 'timezone']
    list_display_links = ['phone_number']
    list_filter = ['operator_code', 'tag']


@admin.register(Mailings)
class MailingsAdmin(admin.ModelAdmin):
    """
    Регистрация и отображения модели Mailings в админке
    """

    list_display = ['short_text', 'start_time', 'end_time']
    list_display_links = ['short_text']
    ordering = ['start_time']
    filter_horizontal = ('tags_filter', 'codes_filter')


@admin.register(Messages)
class MessagesAdmin(admin.ModelAdmin):
    """
    Регистрация и отображения модели Messages в админке
    """

    list_display = ['sending_time', 'message_status', 'client', 'mailing']
    list_display_links = ['sending_time']
    list_filter = ['message_status', 'client']
    ordering = ['sending_time']


@admin.register(Tags)
class TagsAdmin(admin.ModelAdmin):
    """
    Регистрация и отображения модели Tags в админке
    """

    list_display = ['name', 'description']
    list_display_links = ['name']


@admin.register(OperatorsCodes)
class OperatorsCodesAdmin(admin.ModelAdmin):
    """
    Регистрация и отображения модели OperatorsCodes в админке
    """

    list_display = ['code', 'operator']
    list_display_links = ['code']
