from django.apps import AppConfig


class MailingPerformerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailing_core'

    def ready(self):
        import mailing_core.signals
