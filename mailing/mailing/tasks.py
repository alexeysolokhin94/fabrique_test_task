from celery import shared_task
from mailing_core.models import Mailings, Clients, Messages
from mailing_core.utils.message_handler import create_message, send_message


@shared_task()
def init_messages_creation(mailing_id: int) -> None:
    """
    Инициация создания сообщений для рассылки

    :param mailing_id: id_рассылки
    :type mailing_id: int

    :return: None
    """

    mailing = Mailings.objects.get(id=mailing_id)
    tags = mailing.tags_filter.all()
    codes = mailing.codes_filter.all()

    clients = Clients.objects.filter(tag__in=tags).filter(operator_code__in=codes)

    for client in clients:
        new_message = create_message(client, mailing)
        init_sending.apply_async((new_message.id,), expires=mailing.end_time, retry=True,
                                 retry_policy={
                                     'max_retries': 5,
                                 })


@shared_task()
def init_sending(message_id: int) -> None:
    """
    Инициация отправки сообщения

    :param message_id: id сообщения
    :type message_id: int

    :return: None
    """

    message = Messages.objects.get(id=message_id)
    send_message(message)
