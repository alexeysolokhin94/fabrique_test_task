from mailing_core.models import Mailings, Clients
from mailing_api.serializers import ClientSerializer, MailingSerializer
from mailing_api.api_pagination import MailingApiPagination
from rest_framework.viewsets import ModelViewSet


class ClientsApiViewSet(ModelViewSet):
    """
    API клиентов
    """

    queryset = Clients.objects.all()
    pagination_class = MailingApiPagination
    serializer_class = ClientSerializer


class MailingsApiViewSet(ModelViewSet):
    """
    API рассылок
    """

    queryset = Mailings.objects.all()
    pagination_class = MailingApiPagination
    serializer_class = MailingSerializer
