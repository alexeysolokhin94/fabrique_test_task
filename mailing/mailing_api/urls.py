from django.urls import path, include
from mailing_api.api import ClientsApiViewSet, MailingsApiViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('clients', ClientsApiViewSet, basename="client")
router.register('mailings', MailingsApiViewSet, basename="mailing")


urlpatterns = [
    path('', include(router.urls)),
]
