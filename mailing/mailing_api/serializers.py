from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer
from mailing_core.models import Clients, Mailings, Tags, OperatorsCodes


class TagSerializer(serializers.PrimaryKeyRelatedField, serializers.ModelSerializer):
    """
    Сериалайзер модели тэга
    """

    class Meta:
        model = Tags
        fields = ['id', 'name']


class OperatorsCodeSerializer(serializers.PrimaryKeyRelatedField, serializers.ModelSerializer):
    """
    Сериалайзер модели кода оператора
    """

    class Meta:
        model = OperatorsCodes
        fields = ['id', 'code', 'operator']


class ClientSerializer(serializers.ModelSerializer):
    """
    Сериалайзер модели клиента
    """

    class Meta:
        model = Clients
        fields = ['id', 'phone_number', 'operator_code', 'tag', 'timezone']


class MailingSerializer(WritableNestedModelSerializer):
    """
    Сериалайзер модели рассылки
    """

    tags_filter = TagSerializer(many=True, queryset=Tags.objects.all())
    codes_filter = OperatorsCodeSerializer(many=True, queryset=OperatorsCodes.objects.all())

    class Meta:
        model = Mailings
        fields = ['id', 'start_time', 'end_time', 'message_text', 'tags_filter', 'codes_filter']
